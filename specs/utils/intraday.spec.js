import {expect} from 'chai';
import {
  intradayIndex, 
  periodsInTradingDay, 
  convertTimestampToNYHour,
  ticksToEndOfDay,
  startOfTrading,
  endOfTrading,
  indexToTime,
  ticksRemainingToTime,
  timeInFuture,
  dayInFuture,
  dateBasedToken
} from './../../src/utils/intraday';
import moment from 'moment';

describe('Intraday - startOfTrading', () => {
  it('should return a moment obj', () => {
    const timestamp = '1446388500'; //12/1/2016
    const res = startOfTrading(timestamp);
    expect(moment.isMoment(res)).to.equal(true);
  });

  it('should return a 8:30am if formatted', () => {
    const timestamp = '1446388500'; //12/1/2016
    const m = startOfTrading(timestamp);
    const res = m.format('HH:mm');
    expect(res).to.equal('09:30');
  });
});

describe('Intraday - endOfTrading', () => {
  it('should return a moment obj', () => {
    const timestamp = '1446388500'; //12/1/2016
    const res = endOfTrading(timestamp);
    expect(moment.isMoment(res)).to.equal(true);
  });

  it('should return a 04:00pm if formatted', () => {
    const timestamp = '1446388500'; //12/1/2016
    const m = endOfTrading(timestamp);
    const res = m.format('HH:mm')
    expect(res).to.equal('16:00');
  });
});

describe('Intraday - periodsInTradingDay', () => {
  it('should return a number', () => {
    const res = periodsInTradingDay(5);
    expect(typeof res).to.equal('number');
    expect(res).to.equal(78);
  });

  it('should return a number equal to the total number of minutes in a trading day broken down by the number of increments', () => {
    const res = periodsInTradingDay(10);
    expect(res).to.equal(39);
  });
});

describe('Intraday - convertTimestampToNYHour', () => {
  it('should return a moment class', () => {
    const timestamp = '1480872143';
    const res = convertTimestampToNYHour(timestamp);
    expect(moment.isMoment(res)).to.equal(true);
  });

  it('should return a moment class of today', () => {
    const timestamp = '1480872143';
    const res = convertTimestampToNYHour(timestamp);
    const isCurrentDate = res.isSame(new Date(), 'day');
    expect(isCurrentDate).to.equal(true);
  });

  it('should return a moment class of today even if timestamp is in the past', () => {
    const timestamp = '1480613696'; //12/1/2016
    const res = convertTimestampToNYHour(timestamp);
    const isCurrentDate = res.isSame(new Date(), 'day');
    expect(isCurrentDate).to.equal(true);
  });
});

describe('Intraday - intraday index', () => {

  it('should return a number', () => {
    const timestamp = '1480872143';
    const res = intradayIndex(timestamp, 5);
    expect(typeof res).to.equal('number');
  });

  it('should return a 1 if 8:35 and 5 minute increment', () => {
    const timestamp = '1446388500'; //11/1/2016 8:35am CST
    const res = intradayIndex(timestamp, 5);
    expect(res).to.equal(1);
  });

  it('should return a 1 if 8:37 and 5 minute increment', () => {
    const timestamp = '1446388620'; //12/1/2016 8:37am CST
    const res = intradayIndex(timestamp, 5);
    expect(res).to.equal(1);
  });

  it('should return a 1 if 8:38 and 5 minute increment', () => {
    const timestamp = '1446388680'; //12/1/2016 8:38am CST
    const res = intradayIndex(timestamp, 5);
    expect(res).to.equal(1);
  });

  it('should return a 2 if 8:40 and 5 minute increment', () => {
    const timestamp = '1446388800'; //12/1/2016 8:40am CST
    const res = intradayIndex(timestamp, 5);
    expect(res).to.equal(2);
  });

  it('should return a 0 if 8:38 and 10 minute increment', () => {
    const timestamp = '1446388680'; //12/1/2016 8:38am CST
    const res = intradayIndex(timestamp, 10);
    expect(res).to.equal(0);
  });

  it('should return a 1 if 8:40 and 10 minute increment', () => {
    const timestamp = '1446388800'; //12/1/2016 8:40am CST
    const res = intradayIndex(timestamp, 10);
    expect(res).to.equal(1);
  });

  it('should return a 16 if 9:50 and 5 minute increment', () => {
    const timestamp = '1480953000'; //12/5/2016 9:50am CST
    const res = intradayIndex(timestamp, 5);
    expect(res).to.equal(16);
  });
});

describe('Intraday - ticksToEndOfDay', () => {

  it('should return a number', () => {
    const timestamp = '1480872143';
    const res = ticksToEndOfDay(timestamp, 5);
    expect(typeof res).to.equal('number');
  });

  it('should return a 1 if 2:55 and 5 minute increment', () => {
    const timestamp = '1446411300';
    const res = ticksToEndOfDay(timestamp, 5);
    expect(res).to.equal(1);
  });

  it('should return a 0 if 2:55 and 10 minute increment', () => {
    const timestamp = '1446411300';
    const res = ticksToEndOfDay(timestamp, 10);
    expect(res).to.equal(1);
  });

  it('should return a 78 if 8:30am and 5 minute increment', () => {
    const timestamp = '1446388200';
    const res = ticksToEndOfDay(timestamp, 5);
    expect(res).to.equal(78);
  });

  it('should return a 77 if 8:36am and 5 minute increment', () => {
    const timestamp = '1446388560';
    const res = ticksToEndOfDay(timestamp, 5);
    expect(res).to.equal(77);
  });
});

describe('Intraday - indexToTime', () => {
  it('should return a string in HH:mm format', () => {
    const res = indexToTime(1, 5);
    expect(typeof res).to.equal('string');
  });

  it('should return HH:mm equal to ndx * incrment minutes ahead', () => {
    const res = indexToTime(1, 5);
    expect(res).to.equal('09:35');
  });

  it('should return HH:mm equal to ndx * incrment minutes ahead', () => {
    const res = indexToTime(78, 5);
    expect(res).to.equal('16:00');
  });
});

describe('Intraday - ticksRemainingToTime', () => {
  it('should return a string in HHmm format (no colon separator)', () => {
    const res = ticksRemainingToTime(1, 5);
    expect(typeof res).to.equal('string');
  });

  it('should return HH:mm equal to ndx * incrment minutes behind', () => {
    const res = ticksRemainingToTime(1, 5);
    expect(res).to.equal('1555');
  });

  it('should return HH:mm equal to ndx * incrment minutes behind', () => {
    const res = ticksRemainingToTime(78, 5);
    expect(res).to.equal('0930');
  });
});

describe('Intraday - timeInFuture', () => {
  it('should return an object', () => {
    let res = timeInFuture('11-29-2016-12:35', 0, 5, 'MMDDYY-HH:mm');
    expect(typeof res).to.equal('object');
    expect(res).to.have.property('future');
    expect(res).to.have.property('date');
  });

  it('should return a string if prediction pushed into next day', () => {
    let res = timeInFuture('11-29-2016-17:35', 0, 5, 'MMDDYY-HH:mm');
    expect(res.date).to.equal('113016-12:05');
  });

  it('should return the next Monday if prediction pushed into next day from a Friday', () => {
    let res = timeInFuture('12-02-2016-17:35', 0, 5, 'MMDDYY-HH:mm');
    expect(res.date).to.equal('120516-12:05');
  });

  it('should return the future property as true if in future', () => {
    const date = moment().add(1, 'day').format('MM-DD-YYYY-HH:mm');
    let res = timeInFuture(date, 1, 5, 'MMDDYY-HH:mm');    
    expect(res.future).to.equal(true);
  });

  it('should return the future property as false if date has passed', () => {
    const date = moment().subtract(2, 'day').format('MM-DD-YYYY-HH:mm');
    let res = timeInFuture(date, 1, 5, 'MMDDYY-HH:mm');    
    expect(res.future).to.equal(false);
  });

  it('should return the time formatted to NY timezone', () => {
    let res = timeInFuture('11-29-2016-12:35', 0, 5, 'MMDDYY-HH:mm');    
    expect(res.date).to.equal('112916-13:35');
  });

  it('should round the time to a 5min position', () => {
    let res = timeInFuture('11-29-2016-12:34', 0, 5, 'MMDDYY-HH:mm');    
    expect(res.date).to.equal('112916-13:35');
  });

  it('should not round the time if set on a 5min period', () => {
    let res = timeInFuture('11-29-2016-12:35', 0, 5, 'MMDDYY-HH:mm');    
    expect(res.date).to.equal('112916-13:35');
  });

  it('should handle going back in time', () => {
    // 04-18-2017-10:30AM--04:00 -160 5
    let res = timeInFuture('04-18-2017-10:30', -1, 5, 'MMDDYY-HH:mm'); 
    expect(res.date).to.equal('041817-11:25');
  });

});

describe('Helpers - dayInFuture', () => {
  it('should return a object with date and future keys', () => {
    let res = dayInFuture('11292016', 0);    
    expect(typeof res).to.equal('object');
    expect(res).to.have.property('future');
    expect(res).to.have.property('date');
  });

  it('should return date object formatted as a string', () => {
    let res = dayInFuture('11292016', 0);    
    expect(res.date).to.equal('11292016');
  });

  it('should add days depending on the add param value', () => {
    let res = dayInFuture('11292016', 1);    
    expect(res.date).to.equal('11302016');
  });

  it('should return the monday value if a Saturday value is passed in', () => {
    let res = dayInFuture('11262016', 0);    
    expect(res.date).to.equal('11282016');
  });

  it('should return the monday value if a Friday value is passed in but with a 1 add value', () => {
    let res = dayInFuture('11252016', 1);    
    expect(res.date).to.equal('11282016');
  });

  it('should return the future property as true if in future', () => {
    const date = moment().add(1, 'day').format('MMDDYY');
    let res = dayInFuture(date, 1);    
    expect(res.future).to.equal(true);
  });

  it('should return the future property as false if date has passed', () => {
    const date = moment().subtract(2, 'day').format('MMDDYY');
    let res = dayInFuture(date, 1);    
    expect(res.future).to.equal(false);
  });
});

describe('Utils intraday - dateBasedToken', () => {
  it('should return a string token', () => {
   const timestamp = '1487001660';
   const res = dateBasedToken(timestamp, 5);
   expect(typeof res).to.equal('string'); 
  });
});