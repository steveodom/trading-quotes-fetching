import {expect} from 'chai';
import moment from 'moment';

import {
  closestSpecifiedDay
} from './../../src/utils/time';

describe('utils time - closestSpecifiedDay', () => {
  const dayOfWeekNeeded = 5 //Friday
  it('should return a moment object that can be converted to the proper date', () => {
    const m = closestSpecifiedDay(5);
    const res = m.format('YYYY-MM-DD');
    expect(m.isValid()).to.equal(true);
    expect(res).to.equal('2017-06-23');
  });
});
