import {expect} from 'chai';
import fetch, {fetchByDate, matchDate, tradier} from './../../src/quote';
import Promise from 'bluebird';
import moment from 'moment'
import _ from 'lodash';

import {realtime} from './quotes.mock';


const setupDate = (Timestamp) => {
  return moment(Timestamp, 'X');
}

const formatDate = (m, format = 'MM/DD/YYYY hh:mm') => {
  return m.format(format);
}

describe('Quote - fetch', () => {

  it('should return an object with series and meta properties', () => {
    return fetch('aapl', '3y').then( (res, err) => {
        expect(res).to.have.property('series');
        return expect(res).to.have.property('meta');
    });
  });

  it('should return daily if 3 years or less', () => {
    return fetch('aapl', '3y').then( (res, err) => {
        const diff = Math.abs(res.series.length - 755);
        return expect(diff).to.be.below(3);
    });
  });

  it('should return weekly if greater than 3 years', () => {
    return fetch('aapl', '4y').then( (res, err) => {
        return expect(res.series.length).to.equal(209);
    });
  });
});

describe('Quote - fetch daily', () => {
  it('should return an object intraday quotes. Intraday quotes have a Timestamp property rather than Date', () => {
    return fetch('aapl', '1d').then( (res, err) => {
      const quotes = res.series;
      expect(Array.isArray(quotes)).to.equal(true);
      expect(quotes[0]).have.property('Timestamp');
    });
  });

  it('should return intraday quotes that are 1 minutes apart', () => {
    return fetch('aapl', '1d').then( (res, err) => {
      const quotes = res.series;
      const firstQuote = quotes[0];
      const secondQuote = quotes[1];
      const firstQuoteTimestamp = setupDate(firstQuote.Timestamp);
      const secondQuoteTimestamp = setupDate(secondQuote.Timestamp);
      const firstQuoteFuture = firstQuoteTimestamp.add(1, 'm');

      expect(formatDate(firstQuoteFuture)).to.equal(formatDate(secondQuoteTimestamp));
    });
  });

  it('should return intraday quotes that are 5 minutes apart if 2 days', () => {
    return fetch('aapl', '2d').then( (res, err) => {
      const quotes = res.series;
      const firstQuote = quotes[0];
      const secondQuote = quotes[1];
      const firstQuoteTimestamp = setupDate(firstQuote.Timestamp);
      const secondQuoteTimestamp = setupDate(secondQuote.Timestamp);
      const firstQuoteFuture = firstQuoteTimestamp.add(5, 'm');

      expect(formatDate(firstQuoteFuture)).to.equal(formatDate(secondQuoteTimestamp));
    });
  });

  it('should return intraday quotes that are 5 minutes apart if 14 days', () => {
    return fetch('aapl', '14d').then( (res, err) => {
      const quotes = res.series;
      const firstQuote = quotes[0];
      const secondQuote = quotes[1];
      const firstQuoteTimestamp = setupDate(firstQuote.Timestamp);
      const secondQuoteTimestamp = setupDate(secondQuote.Timestamp);
      const firstQuoteFuture = firstQuoteTimestamp.add(5, 'm');

      expect(formatDate(firstQuoteFuture)).to.equal(formatDate(secondQuoteTimestamp));
    });
  });

  it('should return a max of 14 days of intraday data', () => {
    return fetch('aapl', '14d').then( (res, err) => {
      const quotes = res.series;
      const raw = quotes.map( (quote) => {
        return moment(quote.Timestamp, 'X').format('MM/DD/YYYY');
      });
      const days = _.uniq(raw);
      expect(days.length).to.equal(14);
    });
  });

  it('should return a max of 15 days of incomplete intraday data', () => {
    // Notice this is set to 16 days;
    return fetch('aapl', '16d').then( (res, err) => {
      const quotes = res.series;
      const raw = quotes.map( (quote) => {
        return moment(quote.Timestamp, 'X').format('MM/DD/YYYY');
      });
      const days = _.uniq(raw);
      expect(days.length).to.equal(15);
    });
  });

});

describe('Quotes - matchDate', () => {
  
  describe('realtime', () => {
    it('should return an object if found', () => {
      const date = '1483715008'; //9:03
      const res = matchDate(realtime, date);
      expect(typeof res).to.equal('object');  
      expect(res).to.have.property('close');
    });

    it('should return undefined if not found', () => {
      const date = '1'; 
      const res = matchDate(realtime, date);
      expect(res).to.be.undefined;
    });

    it('should return valid quote if found', () => {
      const date = '1483715008'; //9:03
      const res = matchDate(realtime, date);
      expect(res.close).to.equal(117.59);  
    });
  });
  
});



// TODO: these have the possibility of failing depending on the date the tests are run
// IT needs to be MOCKED.
// describe('Quotes - fetch by Date', () => {
//   it('should return an object with the Date object equal to the date passed in', () => {
//     const date = moment().subtract(1, 'd').format('YYYYMMDD');
//     return fetchByDate('aapl', '1m', date).then( (res, err) => {
//       expect(typeof res).to.equal('object');
//       expect(res).to.have.property('Date');
//       expect(res).to.have.property('close');

//       if (res.error) {
//         return expect(res.error).to.equal('not-found');
//       } else {
//         return expect(res.Date.toString()).to.equal(date);
//       }
      
//     }); 
//   });

//   it('should return an object with the Date object equal to the date passed in even if not last date', () => {
//     const date = moment().subtract(2, 'd').format('YYYYMMDD');
//     return fetchByDate('aapl', '1m', date).then( (res, err) => {
//       expect(typeof res).to.equal('object');
//       expect(res).to.have.property('Date');
//       expect(res).to.have.property('close');
//       if (res.error) {
//         return expect(res.error).to.equal('not-found');
//       } else {
//         return expect(res.Date.toString()).to.equal(date);
//       }
//     }); 
//   });

//   it('should return an array if an array of dates is passed in', () => {
//     const date = moment().subtract(2, 'd').format('YYYYMMDD');
//     const date2 = moment().subtract(3, 'd').format('YYYYMMDD');
//     return fetchByDate('aapl', '1m', [date, date2]).then( (res, err) => {
//       expect(Array.isArray(res)).to.equal(true);
//       expect(res[0]).to.have.property('Date');
//       expect(res[0]).to.have.property('close');
//       console.info(res);
//       if (res[0].error) {
//         return expect(res[0].error).to.equal('not-found');
//       } else {
//         return expect(res[0].Date.toString()).to.equal(date);
//       }
//     }); 
//   });

//   it('should return an object with the Timestamp object equal to the date passed in', () => {
//     const date = moment().subtract(10, 'm').format('X');
//     return fetchByDate('aapl', '1d', date).then( (res, err) => {
//       expect(typeof res).to.equal('object');
//       expect(res).to.have.property('Timestamp');
//       expect(res).to.have.property('close');
//       if (res.error) {
//         return expect(res.error).to.equal('not-found');
//       } else {
//         return expect(res.Date.toString()).to.equal(date);
//       }
//     }); 
//   });
// });