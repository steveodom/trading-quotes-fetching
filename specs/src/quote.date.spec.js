import {expect} from 'chai';
import {fetchDateBased} from './../../src/quote';
import Promise from 'bluebird';
import moment from 'moment'
import _ from 'lodash';
import {nockEndpoint} from './../testHelper';

// describe('Quote - fetchDateBased Realtime', () => {
//   const ticker = '.inx';
//   const period = '1d';

  
//   it('should return an object with timestamps as keys', () => {
//     nockEndpoint(ticker, period, 'index');
//     return fetchDateBased(ticker, period).then( (res, err) => {
//       expect(typeof res).to.equal('object');
//       expect(res).to.haveOwnProperty('06232017:50');
//       expect(res).to.haveOwnProperty('06232017:51');
//     });
//   });

//   it('should only return the closest quotes in every 5 minute block', () => {
//     nockEndpoint(ticker, period, 'index');
//     return fetchDateBased(ticker, period).then( (res, err) => {
//       const keys = Object.keys(res);
//       expect(keys.length).to.equal(29);
//     });
//   });
// });

describe('Quote - fetchDateBased Daily', () => {
  const ticker = '.inx';
  const period = '1m';

  it('should return an object with timestamps as keys', () => {
    nockEndpoint(ticker, period, 'daily');
    return fetchDateBased(ticker, period, 'daily').then( (res, err) => {
      expect(typeof res).to.equal('object');
      expect(res).to.haveOwnProperty('06232017');
      expect(res).to.haveOwnProperty('06222017');
      expect(Object.keys(res).length).to.equal(23);
    });
  });

});
