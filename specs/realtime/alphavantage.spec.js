import {expect} from 'chai';
import {alphavantage} from './../../src/realtime/alphavantage'
import Promise from 'bluebird';
import moment from 'moment'
import _ from 'lodash';


describe('AlphaVantage - fetchQuote', () => {

  const res = alphavantage('AAPL', '1d');
  
  it('should return quotes in proper format', () => {
    return res.then( (res) => {
      expect(typeof res).to.equal('object');
      expect(res).haveOwnProperty('meta');
      expect(res).haveOwnProperty('series');

      describe('series', () => {
        const series = res.series;
        const quote = series[0];
        const nextQuote = series[1];
        console.log(quote);
        it('should be an array with correct elements', () => {
          expect(Array.isArray(series)).to.equal(true);
          expect(series.length).to.equal(80);
          expect(typeof quote).to.equal('object');
          expect(quote).haveOwnProperty('Timestamp');
          expect(quote).haveOwnProperty('close');
          expect(_.isNumber(quote.close)).to.equal(true);
          expect(quote).haveOwnProperty('volume');
        });

        it('should be an array with oldest quotes first', () => {
          const quoteTs = moment(quote.Timestamp, 'X');
          const nextQuoteTs = moment(nextQuote.Timestamp, 'X');
          expect((quoteTs).isBefore(nextQuoteTs)).to.equal(true);
        });
      });
    });
  });
});