import {expect} from 'chai';
import {tradier, getQuote} from './../../src/realtime/tradier'
import Promise from 'bluebird';
import moment from 'moment'
import _ from 'lodash';


describe('Tradier - fetchQuote', () => {
  const res = tradier('AAPL');
  
  it('should return quotes in proper format', () => {
    return res.then( (res) => {
      expect(typeof res).to.equal('object');
      expect(res).haveOwnProperty('meta');
      expect(res).haveOwnProperty('series');

      describe('series', () => {
        const series = res.series;
        const quote = series[0];
        const nextQuote = series[1];

        it('should be an array with correct elements', () => {
          expect(Array.isArray(series)).to.equal(true);
          // expect(series.length).to.equal(28);
          expect(typeof quote).to.equal('object');
          expect(quote).haveOwnProperty('Timestamp');
          expect(quote).haveOwnProperty('close');
          expect(quote).haveOwnProperty('volume');
        });

        it('should be an array with oldest quotes first', () => {
          const quoteTs = moment(quote.Timestamp, 'X');
          const nextQuoteTs = moment(nextQuote.Timestamp, 'X');
          expect((quoteTs).isBefore(nextQuoteTs)).to.equal(true);
        });
      });
    });
  });
});

describe('Tradier - get single quote', () => {
  const res = getQuote('AAPL');
  
  it('should return quotes in proper format', () => {
    return res.then( (res) => {
      expect(typeof res).to.equal('object');
      expect(res).haveOwnProperty('close');
      expect(res).haveOwnProperty('Timestamp');
      expect(res).haveOwnProperty('volume');
      expect(res).not.haveOwnProperty('meta');
      expect(res).haveOwnProperty('bid');
      expect(res).haveOwnProperty('ask');
      return expect(res).not.haveOwnProperty('series');
    });
  });
});

describe('Tradier - get single quote with no ticker', () => {
  const res = getQuote();
  
  it('should throw an error with a proper message', () => {
    return res.then( (res) => {
      return expect(false).to.equal(true);
    }).catch( (err) => {
      console.log(err, 'err');
      return expect(typeof err).to.equal('object')
    });
  });
});