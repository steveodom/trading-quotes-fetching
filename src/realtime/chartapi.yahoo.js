import Promise from 'bluebird';
import axios from 'axios';
import moment from 'moment';
import _ from 'lodash';

// 3y seems to be the max history you can get and it be daily sequantially.

// for s&p 500: ^gspc
// nasdaq: ^ixic
// dow jones ^dji
// gold ^gspc
// eg: http://chartapi.finance.yahoo.com/instrument/1.0/%5Egspc/chartdata;type=quote;range=2y;/json?callback=superagentCallback1471552852181179#
const URL = 'http://chartapi.finance.yahoo.com/instrument/1.0/';


// I could also use this endpoint:
// https://query1.finance.yahoo.com/v7/finance/chart/GCZ16.CMX?range=3y&interval=1d&indicators=quote&includeTimestamps=true&includePrePost=false
// it delivers the data in unix epoch timestamps and the data bundled by attributed (eg: close),
// it will only give 18 months or so of data

export const uri = (ticker, period) => {
  return `${URL}${ticker}/chartdata;type=quote;range=${period}/json`;
}

export const hackyJsonpParsing = (res) => {
  return JSON.parse(res.substring(res.indexOf('(') + 1, res.lastIndexOf(')')));
}

export const getSeries = (series, period) => {
  return series;
}

export default function fetch(ticker, period) {
  let adjustedTicker = ticker;
  if (ticker === '.inx') {
    adjustedTicker = '^gspc'
  }
  // return getQuotes(adjustedTicker, period);
  const url = uri(adjustedTicker, period);
  return new Promise((resolve, reject) => {
    axios.get(url)
      .then( (res) => {
        let parsed = hackyJsonpParsing(res.data);
        resolve({meta: parsed.meta, series: getSeries(parsed.series, period)});
      })
      .catch(function (err) {
          console.info('an error occurred', err)
      });
  });
}
