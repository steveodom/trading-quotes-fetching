import Promise from 'bluebird';
import axios from 'axios';
import {closestSpecifiedDay} from './../utils/time';
const TRADIER_ACCESS_TOKEN = 'OapehNEkNeJBqFZPIAeV7rg0BPjG'; //sent by Tradier directly to me.


export const authHeader = {
  headers: {Authorization: `Bearer ${TRADIER_ACCESS_TOKEN}`}
}

export const quoteObject = (quote) =>  {
  return Object.assign({}, {
    Timestamp: quote.timestamp || quote.trade_date,
    open: quote.open,
    close: quote.close || quote.last,
    high: quote.high,
    low: quote.low,
    volume: quote.volume,
    bid: quote.bid,
    ask: quote.ask
  });
};


// Tradier quotes come in asc order (oldest first). This is what trader-features-creator expects
export const parseSeries = (res) => {
  const series = res.map( quoteObject );
  return {series, meta: {}}
}

export const validateQuotesResponse = (res) =>  {
  try {
    return res && res.data && res.data.series && res.data.series.data;
  } catch (error) {
    return {message: 'structure invalid'} 
  }
};


export const tradier = (ticker) =>  {
  const url = `https://api.tradier.com/v1/markets/timesales?symbol=${ticker}&interval=5min&session_filter=open`
  return new Promise((resolve, reject) => {
    axios.get(url, authHeader)
    .then( validateQuotesResponse )
    .then( parseSeries )
    .then( (res) => ( resolve(res) ))
    .catch(function (err) {
        console.info('an error occurred', err)
    })
  })
};


export const validateQuoteResponse = (res, reject) =>  {
  if (!res || res.hasOwnProperty('unmatched_symbols')) {
    return reject({message: 'ticker not found'});
  }
  return res;
};

export const getQuoteObject = (res) =>  {
  try {
    return res && res.data && res.data.quotes && res.data.quotes.quote;
  } catch (error) {
    return {message: 'structure invalid'} 
  }
};


export const getQuote = (ticker) =>  {
  const url = `https://api.tradier.com/v1/markets/quotes?symbols=${ticker}`
  return new Promise((resolve, reject) => {
    axios.get(url, authHeader)
    .then( getQuoteObject )
    .then( (res) => validateQuoteResponse(res, reject) )
    .then( quoteObject )
    .then( (res) => ( resolve(res) )) 
    .catch( (err) =>  {
      return reject(err)
    });
  });
};



export const getOptionQuotes = (ticker) =>  {
  const nearestExpirationMoment = closestSpecifiedDay(5); // 5 is friday; returns the next friday if today is friday
  const nearestExpiration = nearestExpirationMoment.format('YYYY-MM-DD');
  const url = `https://sandbox.tradier.com/v1/markets/options/chains?symbol=${ticker}&expiration=${nearestExpiration}`
  return new Promise((resolve, reject) => {
    axios.get(url, {
      headers: {Authorization: `Bearer ${TRADIER_ACCESS_TOKEN}`}
    })
    .then( (res) => {
      if (res && res.data && res.data.options && res.data.options.option) {
        const filtered = res.data.options.option.filter( (option) => (option.open_interest > 0));
        resolve(filtered);
      } else {
        reject('quote response is not right structure')
      }
      
    })
    .catch(function (err) {
        console.info('an error occurred', err)
    });
  });
};

export default tradier;