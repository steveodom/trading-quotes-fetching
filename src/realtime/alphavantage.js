import Promise from 'bluebird';
import axios from 'axios';
import moment from 'moment-timezone';
export const ALPHAVANTAGE_KEY = '0WY3';

// Returns quotes most recent first. trading-features-creator expects oldest first. we REVERSE here:
export const parse = (res, period) => {
  const ary = [];
  const series = Object.keys(res).forEach( (key, i) => {
    const quote = res[key];
    const obj =  Object.assign({}, {
      Timestamp: moment.tz(key, 'YYYY-MM-DD HH:mm::ss', 'America/New_York').format('X'),
      open: parseFloat(quote['1. open']),
      close: parseFloat(quote['4. close']),
      high: parseFloat(quote['2. high']),
      low: parseFloat(quote['3. low']),
      volume: parseFloat(quote['5. volume']),
      key
    });
    if (period === '1d' && i < 80) {
      ary.push(obj);
    }

    if (period !== '1d') {
      ary.push(obj)
    }
  });
  return {series: ary.reverse(), meta: {}}
}

export const buildURL = (ticker, period) => {
  // outputsize: full = a lot of data ~ 2 months : compact last 100 datapoints
  const isDaily = (period === '3y' || period === '1m');
  const needsLimitedData = (period === '1d' || period === '1m');
  const outputsize = needsLimitedData ? 'compact' : 'full';
  const endpoint = isDaily ? 'DAILY' : 'INTRADAY';
  const timeSeries = isDaily ? 'Daily' : '5min'; 
  return {
    path: `query?function=TIME_SERIES_${endpoint}&symbol=${ticker}&interval=5min&apikey=${ALPHAVANTAGE_KEY}&outputsize=${outputsize}`,
    timeSeries
  }
}

export const alphavantage = (ticker, period) =>  {
  const {path, timeSeries} = buildURL(ticker, period);
  const url = `http://www.alphavantage.co/${path}`
  return new Promise((resolve, reject) => {
    axios.get(url)
    .then( (res) => {
      if (res && res.data && res.data[`Time Series (${timeSeries})`]) {
        let quotes = parse(res.data[`Time Series (${timeSeries})`], period);
        resolve({meta: quotes.meta, series: quotes.series});
      } else {
        reject({klass: 'quote', source: 'alphavantage', msg: res.data});
      }
    })
    .catch(function (err) {
        console.info('an error occurred', err)
    });
  });
};

export default alphavantage;