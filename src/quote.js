import Promise from 'bluebird';
import axios from 'axios';
import moment from 'moment';
import _ from 'lodash';
import {dateBasedToken} from './utils/intraday';
import {dates} from 'namer';
import getQuotes from './realtime/alphavantage';
import {getOptionQuotes, getQuote} from './realtime/tradier';

// sometimes (in feature-creator) aliased as getQuotes
export default function fetch(ticker, period) {
  return getQuotes(ticker, period);
}

export function fetchOptionQuotes(ticker, period) {
  return getOptionQuotes(ticker, period);
};

export const fetchLatestQuote = (ticker) =>  {
  return getQuote(ticker);
};



export function matchDate(series, date, frequency = 'realtime') {
  if (_.isEmpty(series)) {
    return
  } else {
    if (frequency === 'realtime') {
      const mTarget = moment(date, 'X');
      const modified = series.map( (quote) => {
        const aQuote = moment(quote.Timestamp.toString(), 'X');
        const diff = mTarget.diff(aQuote); 
        
        return Object.assign({}, quote, {
          human: aQuote.format('MM/DD/YY HH:mm Z'),
          diff: Math.abs(moment.duration(diff).asSeconds())
        });
      });

      const sorted = _.sortBy(modified, (a) => ( a.diff ) );
      const closest = sorted[0];
      const targetHuman = mTarget.format('MM/DD/YY HH:mm Z');
      // console.info('closest', closest, targetHuman);
      if ( closest.diff < 90 ) {
        return closest;
      } else {
        return;
      }
    } else {
      return series.find( (quote) => (quote.key.toString() === date));
    }
  }
}

export function findClosestQuotes(series, dateArray, frequency) {
  return dateArray.map( (d) => {
    const match = matchDate(series, d, frequency);  
    return match ? match : emptyQuote;  
  });
}

const emptyQuote = {
  Timestamp: 0,
  Date: 19700101,
  close: 0,
  error: 'not-found'
} 

export function fetchByDate(ticker, period, date, frequency = 'realtime') {
  return fetch(ticker, period).then((quotes) => {
    let match;    
  
    // the date arg can be an array of dates to return
    if (Array.isArray(date)) {
      if (_.isEmpty(quotes.series)) return [emptyQuote, emptyQuote];
      const targets = findClosestQuotes(quotes.series, date, frequency);
      return Promise.resolve(targets);
    } else {
      if (_.isEmpty(quotes.series)) return emptyQuote;
      match = matchDate(quotes.series, date);  
      if (match) {
        Promise.resolve(match);
      } else {
        return Promise.resolve(emptyQuote);
      }
    } 
  });
}

export const fetchDateBased = (ticker, period, frequency = 'realtime') =>  {
  return fetch(ticker, period).then((quotes) => {
    const obj = {};
    quotes.series.forEach( (quote) => {
      let token;

      if (frequency === 'realtime') {
        token = dateBasedToken(quote.Timestamp, 5);
      } else {
        token = dates.display(quote.key, 'daily', 'alphavantage', 'output', false) 
      }
      obj[token] = quote;
    })

    return Promise.resolve(obj); 
  });
};
