import Promise from 'bluebird';
import axios from 'axios';

export const uri = (ticker, startDate='2006-04-21') => {
  return `https://www.quandl.com/api/v3/datasets/CHRIS/${ticker}.json?api_key=4Xejve9ZwEfPqnx-ZkA1&start_date=${startDate}`;
}


export const cleanUp = (raw_data) => {
  const meta = {};
  const exchangeDetails = raw_data.dataset_code.split('_');
  meta.exchange = exchangeDetails[0];
  meta.ticker = exchangeDetails[1];

  const series = raw_data.data.map( (ary) => {
    return {
      Date: ary[0].replace('_', ''),
      close: ary[4],
      high: ary[2],
      low: ary[3],
      volume: ary[7]
    }
  });
  return {meta, series};
}

const fetch = (ticker, startDate) => {
  const url = uri(ticker, startDate);

  return new Promise((resolve, reject) => {
    axios.get(url)
      .then( (res) => {
        if (res.data && res.data.dataset) {
          resolve(cleanUp(res.data.dataset));
        } else {
          reject({error: 'unknown data structure'})
        }

      })
      .catch(function (err) {
          console.info('an error occurred', err)
      });
  })
}

export default fetch;
