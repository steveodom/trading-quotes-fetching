import moment from 'moment';
import _ from 'lodash';
import {makeWeekday} from './time';
import {dates} from 'namer';

const EMPTY_TIMESTAMP = () => {
  return _.round(Date.now() / 1000);
}

// returns todays date. Not the date of the timestamp. It's ok if we are
// only building intradayIndex and ticksToEndOfDay'

const dateStringFormat = dates.formats.realtime.output;

export const startOfTrading = (timestamp) => {
  const day = convertTimestampToNYHour(timestamp); 
  const start = day.format('MM/DD/YYYY');
  return moment(`${start} 09:30`, dateStringFormat);
};

export const endOfTrading = (timestamp) => {
  const day = convertTimestampToNYHour(timestamp); 
  const start = day.format('MM/DD/YYYY');
  return moment(`${start} 16:00`, dateStringFormat);
};

export const periodsInTradingDay = (increment = '5') => {
  const start = startOfTrading(EMPTY_TIMESTAMP); //8:30am GMT;
  const fin = endOfTrading(EMPTY_TIMESTAMP);
  const diff = fin.diff(start, 'minutes');
  return diff / increment;
}

export const convertTimestampToNYHour = (timestamp) => {
  const m = dates.nyDateM(timestamp, 'X');
  const hour = m.format('HH:mm a');
  return moment(hour, 'hh:mm a');
}

export const intradayIndex = (timestamp, increment = 5) => {
  const start = startOfTrading(timestamp); //8:30am GMT;
  const trade = convertTimestampToNYHour(timestamp);
  const diff = trade.diff(start, 'minutes');
  return _.floor(diff / increment, 0);
}

export const dateBasedToken = (timestamp, increment = 5) => {
  const indx = intradayIndex(timestamp, increment);
  const timeString = moment(timestamp, 'X').format('MMDDYYYY');
  return `${timeString}:${indx}`;
}


export const indexToTime = (ndx, increment = 5) => {
  const start = startOfTrading(EMPTY_TIMESTAMP); //8:30am GMT;
  const m = start.add(ndx * increment, 'm');
  return m.format('HH:mm');
}

export const ticksRemainingToTime = (ndx, increment = 5) => {
  const start = endOfTrading(EMPTY_TIMESTAMP); //8:30am GMT;
  const m = start.subtract(ndx * increment, 'm');
  return m.format('HHmm');
}

export const ticksToEndOfDay = (timestamp, increment = 5) => {
  const fin = endOfTrading(timestamp);
  const trade = convertTimestampToNYHour(timestamp);
  const diff = fin.diff(trade, 'minutes');
  return _.ceil(diff / increment, 0);
}
 
export const timeInFuture = (date, n, increment=5, format='X') => {
  let ny = dates.nyDateM(date, dateStringFormat, true)
  let m = ny.add(n * increment, 'm');
  let timestamp = m.format('X');
  const eod = ticksToEndOfDay(timestamp,5);
  const beforeTodaysClose = eod > 0; 
  
  if (beforeTodaysClose) {
    return {date: m.format(format), future: m.isSameOrAfter()};
  } else {
    let nextTradingDay = moment(date, 'MM-DD-YYYY-HH:mm').add(1, 'd');
    nextTradingDay = makeWeekday(nextTradingDay);
    const hhmm = indexToTime(eod * -1, increment);
    m = moment(`${nextTradingDay.format('MM/DD/YYYY')} ${hhmm}`, 'MM/DD/YYYY HH:mm');
    return {date: m.format(format), future: m.isSameOrAfter()};
  }
}

export const dayInFuture = (date, add, format = 'MMDDYYYY') => {
  let m = moment(date, 'MMDDYYYY').add(add, 'days');
  
  if (m.weekday() > 5) {
    let monday = m.weekday() === 7 ? 1 : 2;
    m = m.add(monday, 'days');
  };
  return {date: m.format(format), future: m.isAfter()};
}