import moment from 'moment';

export const makeWeekday = (m) => {
  if (m.weekday() > 5) {
    let monday = m.weekday() === 7 ? 1 : 2;
    m = m.add(monday, 'days');
  }
  return m;
}

export const closestSpecifiedDay = (dayOfWeekNumber = 5) => {
  // if we haven't yet passed the day of the week that I need:
  if (moment().isoWeekday() <= dayOfWeekNumber) { 
    // then just give me this week's instance of that day
    return moment().isoWeekday(dayOfWeekNumber);
  } else {
    // otherwise, give me next week's instance of that day
    return moment().add(1, 'weeks').isoWeekday(dayOfWeekNumber);
  }
}
